CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module has a block to display the Title and Description of the taxonomy according to the category parameter that must be passed in the url


REQUIREMENTS
------------

* This module don't need requirements because Taxonomy module is inside Drupal core.


INSTALLATION
------------

 * Install the Block Show Taxonomy Title module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420
   for further information.


CONFIGURATION
-------------

 * You must put the block on the page of a taxonomy that it will automatically get the title and description.


MAINTAINERS
-----------

 * Ilgner Fagundes (ilgnerfagundes) - https://www.drupal.org/u/ilgnerfagundes

Supporting organization:

 * CI&T - https://www.drupal.org/cit
